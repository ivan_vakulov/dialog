﻿using Dialog.DBModels;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace WCF.Dialog.DialogServiceInterface
{
    [ServiceContract]
    public interface IDialogContract
    {
        [OperationContract]
        bool UserExists(string login);
        [OperationContract]
        User GetUserByLogin(string login);
        [OperationContract]
        void AddUser(User user);
        [OperationContract]
        void AddDialog(DialogText dialog);
        [OperationContract]
        List<DialogText> GetDialogsByUserGuid(Guid userGuid);
    }
}
