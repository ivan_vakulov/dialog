﻿using Dialog.DBModels;
using WCF.Dialog.DialogServiceInterface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.ServiceModel;

namespace WCF.Dialog.DialogService
{
    public class DialogServiceWrapper
    {
        public static bool UserExists(string login)
        {
            using (var myChannelFactory = new ChannelFactory<IDialogContract>("Server"))
            {
                IDialogContract client = myChannelFactory.CreateChannel();
                return client.UserExists(login);
            }
        }

        public static User GetUserByLogin(string login)
        {
            using (var myChannelFactory = new ChannelFactory<IDialogContract>("Server"))
            {
                IDialogContract client = myChannelFactory.CreateChannel();
                return client.GetUserByLogin(login);
            }
        }

        public static void AddUser(User user)
        {
            try
            {
                using (var myChannelFactory = new ChannelFactory<IDialogContract>("Server"))
                {
                    IDialogContract client = myChannelFactory.CreateChannel();
                    client.AddUser(user);
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public static void AddDialog(DialogText dialog)
        {
            try
            {
                using (var myChannelFactory = new ChannelFactory<IDialogContract>("Server"))
                {
                    IDialogContract client = myChannelFactory.CreateChannel();
                    client.AddDialog(dialog);
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public static List<DialogText> GetDialogsByUserGuid(Guid userGuid)
        {
            using (var myChannelFactory = new ChannelFactory<IDialogContract>("Server"))
            {
                IDialogContract client = myChannelFactory.CreateChannel();
                return client.GetDialogsByUserGuid(userGuid);
            }
        }
    }
}
