﻿using Dialog.DBModels;
using System;
using System.Collections.Generic;
using WCF.Dialog.DialogService;

namespace Dialog.Managers
{
    public static class DBManager
    {
        public static bool UserExists(string login)
        {
            return DialogServiceWrapper.UserExists(login);
        }

        public static User GetUserByLogin(string login)
        {
            return DialogServiceWrapper.GetUserByLogin(login);
        }

        public static void AddUser(User user)
        {
            DialogServiceWrapper.AddUser(user);
        }

        public static void AddDialog(DialogText dialog)
        {
            DialogServiceWrapper.AddDialog(dialog);
        }

        public static List<DialogText> GetDialogsByUserGuid(Guid UserGuid)
        {
            return DialogServiceWrapper.GetDialogsByUserGuid(UserGuid);
        }
    }
}
