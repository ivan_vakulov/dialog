﻿using Dialog.DBModels;
using Dialog.Tools;
using System;
using System.IO;
using System.Windows;

namespace Dialog.Managers
{
    public static class StationManager
    {
        public static User CurrentUser { get; set; }

        static StationManager()
        {
            DeserializeLastUser();
        }

        public static void SerializeLastUser()
        {
            try
            {
                SerializationManager.Serialize<User>(CurrentUser, Path.Combine(FileFolderHelper.LastUserFilePath));
            }
            catch (Exception ex)
            {
                Logger.Log("Failed to Seserialize last user", ex);
            }
        }

        private static void DeserializeLastUser()
        {
            User userCandidate;
            try
            {
                userCandidate = SerializationManager.Deserialize<User>(Path.Combine(FileFolderHelper.LastUserFilePath));
            }
            catch (Exception ex)
            {
                userCandidate = null;
                Logger.Log("Failed to Deserialize last user", ex);
            }
            if (userCandidate == null)
            {
                Logger.Log("User was not deserialized");
                return;
            }
            CurrentUser = userCandidate;
        }

        public static void RemoveCurrentUser()
        {
            if (File.Exists(FileFolderHelper.LastUserFilePath)) File.Delete(FileFolderHelper.LastUserFilePath);
            CurrentUser = null;
        }

        public static void CloseApp()
        {
            MessageBox.Show("ShutDown");
            Environment.Exit(1);
        }
    }
}
