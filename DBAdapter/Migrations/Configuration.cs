﻿using System.Data.Entity.Migrations;

namespace Dialog.DBAdapter.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DialogDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "DialogDBContext";
        }

        protected override void Seed(DialogDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
