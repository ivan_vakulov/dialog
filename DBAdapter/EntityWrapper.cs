﻿using Dialog.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.DBAdapter
{
     public static class EntityWrapper
    {
        public static List<DialogText> GetDialogsByUserGuid(Guid userGuid)
        {
            using (var context = new DialogDBContext())
            {
                return context.Dialogs.Where(x => x.UserGuid == userGuid).ToList();
            }
        }

        public static void AddDialog(DialogText dialog)
        {
            using (var context = new DialogDBContext())
            {
                dialog.DeleteDatabaseValues();
                context.Dialogs.Add(dialog);
                context.SaveChanges();
            }
        }

        public static void AddUser(User user)
        {
            using (var context = new DialogDBContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public static User GetUserByLogin(string login)
        {
            using (var context = new DialogDBContext())
            {
                return context.Users.FirstOrDefault(u => u.Login == login);
            }
        }

        public static bool UserExists(string login)
        {
            using (var context = new DialogDBContext())
            {
                return context.Users.Any(u => u.Login == login);
            }
        }
    }
}
