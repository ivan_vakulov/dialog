﻿using Dialog.DBAdapter.Migrations;
using Dialog.DBModels;
using System.Data.Entity;

namespace Dialog.DBAdapter
{
    public class DialogDBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<DialogText> Dialogs { get; set; }

        public DialogDBContext() : base("NewDialogDB")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DialogDBContext, Configuration>(true));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new User.UserEntityConfiguration());
            modelBuilder.Configurations.Add(new DialogText.TranslitEntityConfiguration());
        }
    }
}
