﻿using Dialog.Tools;
using System;
using System.ServiceModel;
using System.ServiceProcess;

namespace WCF.Dialog.DialogService
{
    class DialogWindowsService : ServiceBase
    {
        internal const string CurrentServiceName = "DialogService";
        internal const string CurrentServiceDisplayName = "Dialog Service";
        internal const string CurrentServiceSource = "DialogService";
        internal const string CurrentServiceLogName = "DialogServiceLogName";
        internal const string CurrentServiceDescription = "Dialog for learning purposes.";
        private ServiceHost _serviceHost = null;

        public DialogWindowsService()
        {
            ServiceName = CurrentServiceName;
            try
            {
                AppDomain.CurrentDomain.UnhandledException += UnhandledException;
                Logger.Log("Initialization");
            }
            catch (Exception ex)
            {
                Logger.Log("Initialization", ex);
            }
        }

        protected override void OnStart(string[] args)
        {
            Logger.Log("OnStart");
            RequestAdditionalTime(120 * 1000);
            try
            {
                if (_serviceHost != null)
                    _serviceHost.Close();
            }
            catch
            {
            }
            try
            {
                _serviceHost = new ServiceHost(typeof(DialogService));
                _serviceHost.Open();
            }
            catch (Exception ex)
            {
                Logger.Log("OnStart: " + ex.ToString());
                throw;
            }
            Logger.Log("Service Started ");
        }

        protected override void OnStop()
        {
            Logger.Log("OnStop");
            RequestAdditionalTime(120 * 1000);
            try
            {
                _serviceHost.Close();
            }
            catch (Exception ex)
            {
                Logger.Log("Trying To Stop The Host Listener: " + ex.ToString());
            }
            Logger.Log("Service Stopped");
        }

        private void UnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var ex = (Exception)args.ExceptionObject;

            Logger.Log("UnhandledException: " + ex.ToString());
        }
    }
}
