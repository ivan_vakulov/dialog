﻿using Dialog.DBAdapter;
using Dialog.DBModels;
using System;
using System.Collections.Generic;
using WCF.Dialog.DialogServiceInterface;

namespace WCF.Dialog.DialogService
{
    class DialogService : IDialogContract
    {
        public bool UserExists(string login)
        {
            return EntityWrapper.UserExists(login);
        }

        public User GetUserByLogin(string login)
        {
            return EntityWrapper.GetUserByLogin(login);
        }

        public void AddUser(User user)
        {
            EntityWrapper.AddUser(user);
        }

        public void AddDialog(DialogText dialog)
        {
            EntityWrapper.AddDialog(dialog);
        }

        public List<DialogText> GetDialogsByUserGuid(Guid UserGuid)
        {
            return EntityWrapper.GetDialogsByUserGuid(UserGuid);
        }
    }
}
