﻿using Dialog.ViewModels;
using System.Windows.Controls;

namespace Dialog.Views
{
    /// <summary>
    /// Логика взаимодействия для MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        private MainViewViewModel _mainWindowViewModel;
        public MainView()
        {
            InitializeComponent();
            _mainWindowViewModel = new MainViewViewModel();
            DataContext = _mainWindowViewModel;
        }
    }
}
