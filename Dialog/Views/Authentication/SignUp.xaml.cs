﻿using Dialog.ViewModels;

namespace Dialog.Views.Authentication
{
    /// <summary>
    /// Логика взаимодействия для SignUp.xaml
    /// </summary>
    public partial class SignUp
    {
        #region Constructor
        public SignUp()
        {
            InitializeComponent();
            var signUpViewModel = new SignUpViewModel();
            DataContext = signUpViewModel;
        }
        #endregion
    }
}
