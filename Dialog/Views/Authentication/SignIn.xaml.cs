﻿using Dialog.ViewModels;

namespace Dialog.Views.Authentication
{
    /// <summary>
    /// Логика взаимодействия для SignIn.xaml
    /// </summary>
    public partial class SignIn
    {
        #region Constructor
        public SignIn()
        {
            InitializeComponent();
            var signInViewModel = new SignInViewModel();
            DataContext = signInViewModel;
        }
        #endregion
    }
}
