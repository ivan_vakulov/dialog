﻿using Dialog.DBModels;
using Dialog.Managers;
using Dialog.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using static Dialog.Properties.Resources;

namespace Dialog.ViewModels
{
    public class MainViewViewModel : INotifyPropertyChanged
    {
        private DialogText _selectedDialog;
        private string _textFromDialog;
        private string _textToDialog;
        private List<DialogText> _dialogs;

        public DialogText SelectedDialog
        {
            get => _selectedDialog;
            set
            {
                _selectedDialog = value;
                OnPropertyChanged();
            }
        }
        public List<DialogText> Dialogs
        {
            get => _dialogs;
            set
            {
                _dialogs = value;
                OnPropertyChanged();
            }
        }
        public string TextFromDialog
        {
            get => _textFromDialog;
            set
            {
                _textFromDialog = value;
                OnPropertyChanged();
            }
        }
        public string TextToDialog
        {
            get => _textToDialog;
            set
            {
                _textToDialog = value;
                OnPropertyChanged();
            }
        }

        private ICommand _closeCommand;
        private ICommand _logOutCommand;
        private ICommand _loadDialogsCommand;
        private ICommand _sendToRobotCommand;

        public ICommand CloseCommand
        {
            get { return _closeCommand ?? (_closeCommand = new RelayCommand<object>(CloseExecute)); }
        }
        public ICommand LogOutCommand
        {
            get { return _logOutCommand ?? (_logOutCommand = new RelayCommand<object>(LogOutExecute)); }
        }
        public ICommand LoadDialogsCommand
        {
            get { return _loadDialogsCommand ?? (_loadDialogsCommand = new RelayCommand<object>(LoadDialogsExecute)); }
        }
        public ICommand SendToRobotCommand
        {
            get { return _sendToRobotCommand ?? (_sendToRobotCommand = new RelayCommand<object>(SendTextToChat)); }
        }


        public MainViewViewModel()
        {
            TextFromDialog = DialogVocabular.StartDialog();
        }

        private async void SendTextToChat(object o)
        {
            TextToDialog += "\r\n";
            TextFromDialog += TextToDialog;
            LoaderManager.Instance.ShowLoader();
            var result = await Task.Run(() =>
            {
                if(_textToDialog == string.Empty)
                {
                    TextFromDialog += "Please, talk to me..\r\n";
                    return true;
                }
                if (_textToDialog.Contains("?"))
                {
                    TextFromDialog += DialogVocabular.GetAnswer();
                    TextFromDialog += "\r\n";
                }
                else
                {
                    TextFromDialog += DialogVocabular.GetQuestion();
                    TextFromDialog += "\r\n";
                }
                TextToDialog = string.Empty;
                return true;
            });
            LoaderManager.Instance.HideLoader();
        }

        private async void LoadDialogsExecute(object o)
        {
            LoaderManager.Instance.ShowLoader();
            var result = await Task.Run(() =>
            {
                try
                {
                    Logger.Log("User ask DB for own dialogs.");
                    Dialogs = DBManager.GetDialogsByUserGuid(StationManager.CurrentUser.Guid);
                }
                catch (Exception e)
                {
                    Logger.Log("Error while get dialogs: " + e.ToString());
                    return false;
                }
                return true;
            });
            LoaderManager.Instance.HideLoader();
        }

        private async void LogOutExecute(object obj)
        {
            LoaderManager.Instance.ShowLoader();
            var result = await Task.Run(() =>
            {
                try
                {
                    Logger.Log("User want to log out.");
                    Logger.Log("Clear current user for auto login.");
                    var d = new DialogText(StationManager.CurrentUser, _textFromDialog);
                    DBManager.AddDialog(d);
                    StationManager.RemoveCurrentUser();
                    Logger.Log("Now another user can Sign In.");
                }
                catch (Exception e)
                {
                    Logger.Log("Error while user log out: " + e.ToString());
                    return false;
                }
                return true;
            });
            LoaderManager.Instance.HideLoader();
            if (result) NavigationManager.Instance.Navigate(ModesEnum.SignIn);
        }

        private void ShowSelectedDialog(DialogText dialog)
        {
            TextFromDialog = dialog.Dialog;
        }

        private void CloseExecute(object obj)
        {
            var d = new DialogText(StationManager.CurrentUser, _textFromDialog);
            DBManager.AddDialog(d);
            StationManager.CloseApp();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "SelectedDialog") ShowSelectedDialog(_selectedDialog);
        }

        [NotifyPropertyChangedInvocator]
        public virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (propertyName == "SelectedDialog")
            {
                ShowSelectedDialog(_selectedDialog);
            }
            else
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
