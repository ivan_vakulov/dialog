﻿using System.Windows.Controls;

namespace Dialog.Tools
{
    internal interface IContentWindow
    {
        ContentControl ContentControl { get; }
    }
}
