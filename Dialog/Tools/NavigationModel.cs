﻿using Dialog.Views;
using Dialog.Views.Authentication;
using System;

namespace Dialog.Tools
{
    internal enum ModesEnum
    {
        SignIn,
        SingUp,
        Main
    }

    internal class NavigationModel
    {
        private readonly IContentWindow _contentWindow;
        private SignIn _signIn;
        private SignUp _signUp;
        private MainView _mainView;

        internal NavigationModel(IContentWindow contentWindow)
        {
            _contentWindow = contentWindow;
        }

        internal void Navigate(ModesEnum mode)
        {
            switch (mode)
            {
                case ModesEnum.SignIn:
                    _contentWindow.ContentControl.Content = _signIn = new SignIn();
                    break;
                case ModesEnum.SingUp:
                    _contentWindow.ContentControl.Content = _signUp = new SignUp();
                    break;
                case ModesEnum.Main:
                    _contentWindow.ContentControl.Content = _mainView = new MainView();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }
        }
    }
}
