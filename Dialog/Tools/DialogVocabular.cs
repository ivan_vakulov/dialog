﻿using Dialog.Managers;
using System;

namespace Dialog.Tools
{
    public static class DialogVocabular
    {
        private static Random r = new Random();
        private static bool AutoLogginUser = false;

        public static void CheckFroStartDialog(bool flag)
        {
            AutoLogginUser = flag;
        }

        static string[] answers =
             {
                "Nice",
                "Continue please",
                "It's funny",
                "Yes, sure",
                "I have to think",
                "Well, it's hard to say",
                "Why are you asking this question?",
                "Who knows",
                "I'm not sure",
                "Let's change topic. I don't want to answer this question",
                "I disagree with you"
            };

        static string[] questions =
        {
                "Can you tell me more about you?",
                "It's interesting, isn't it?",
                "What happened next?",
                "Do you really think so?",
                "Are you sure about that?",
                "But why?"
            };

        public static string StartDialog()
        {
            if (AutoLogginUser)
            {
                return "Hi " + StationManager.CurrentUser.FirstName + ", what's new?\r\n";
            }
            return "Hello! My name is GG. Let's talk about something.\r\n";
        }

        public static string GetAnswer()
        {
            return answers[r.Next(answers.Length)];
        }

        public static string GetQuestion()
        {
            return questions[r.Next(questions.Length)];
        }
    }
}
