﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;

namespace Dialog.DBModels
{
    [DataContract(IsReference = true)]
    public class DialogText
    {
        [DataMember]
        private Guid _guid;
        [DataMember]
        private Guid _userGuid;        
        [DataMember]
        private string _dialogText;
        [DataMember]
        private DateTime _date;
        [DataMember]
        private User _user;

        [Key]
        public Guid Guid
        {
            get => _guid;
            private set { _guid = value; }
        }

        public Guid UserGuid
        {
            get => _userGuid;
            private set { _userGuid = value; }
        }

        public string Dialog
        {
            get => _dialogText;
            set { _dialogText = value; }
        }

        public DateTime Date
        {
            get => _date;
            set { _date = value; }
        }

        public User User
        {
            get => _user;
            set { _user = value; }
        }

        public DialogText() { }

        public DialogText(User user, string dialogText) : this()
        {
            _guid = Guid.NewGuid();
            _userGuid = user.Guid;
            _dialogText = dialogText;
            _date = DateTime.Now;
            _user = user;
            user.Dialogs.Add(this);
        }

        public override string ToString()
        {
            return this.Date.ToString();
        }

        public class TranslitEntityConfiguration : EntityTypeConfiguration<DialogText>
        {
            public TranslitEntityConfiguration()
            {
                ToTable("Dialogs");
                HasKey(k => k.Guid);

                Property(p => p.Guid)
                    .HasColumnName("Guid")
                    .IsRequired();
                Property(p => p.Dialog)
                    .HasColumnName("DialogText")
                    .IsRequired();
                Property(p => p.Date)
                    .HasColumnName("Date")
                    .IsRequired();
            }
        }

        public void DeleteDatabaseValues()
        {
            _user = null;
        }
    }
}